<?php

/**
 * @file node_gallery_access.install
 * 
 * Node gallery access install file. 
 */
 
/**
 * Implementation of hook_schema()
 */
function node_gallery_access_schema() {
  $schema = array();
  $schema['node_gallery_access'] = array(
    'fields' => array(
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Gallery node id.'),
      ),
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Gallery node uid.'),
      ),
      'access_type' => array(
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'password' => array(
        'description' => t('The sha1 hashed password for the given node.'),
        'type' => 'varchar',
        'length' => 40,
        'not null' => TRUE,
        'default' => ''
      ),
    ),
    'unique keys' => array(
      'nid' => array('nid'),
    ),
  );
  
  return $schema;
}

/**
 * Implementation of hook_requirements()
 */
function node_gallery_access_requirements($phase) {
  $requirements = array();
  $t = get_t();

  switch ($phase) {
    case 'install':
      $error = FALSE;
      // Code borrowed from: http://www.drupaler.co.uk/blog/using-hookrequirements-drupal-6x/135
      // We need to make sure that node_gallery is installed AND enabled before we install.
      if (!module_exists('node_gallery')) {
        $error = TRUE;
        $value = $t('Node Gallery module needs to be pre-installed.');
        $severity = REQUIREMENT_ERROR;
      }
     
      if ($error) {
        $requirements['node_gallery_access'] = array(
          'title' => $t('Node Gallery Access requirements'),
          'description' => $value . $t(' If the required modules are now installed, please enable Node Gallery Access again.'),
          'severity' => $severity,
        );
      }
    break;
  }

  return $requirements;
}

function node_gallery_access_install_default_type() {
    $gallery_types = drupal_map_assoc(node_gallery_get_types());
    if (array_key_exists('node_gallery_gallery', $gallery_types)) {
      variable_set('node_gallery_access_types', array('node_gallery_gallery', 'node_gallery_gallery'));
    }
}

/**
 * Implementation of hook_install()
 */
function node_gallery_access_install() {
  drupal_install_schema('node_gallery_access');
  $weight = db_result(db_query("SELECT weight FROM {system} WHERE name = 'node_gallery' AND type = 'module'"));
  db_query("UPDATE {system} SET weight = %d WHERE name = 'node_gallery_access' AND type = 'module'", $weight + 1);
  node_gallery_access_install_default_type();
}

/**
 * Implementation of hook_uninstall()
 */
function node_gallery_access_uninstall() {
  global $conf;
  drupal_uninstall_schema('node_gallery_access');
  //Clean up our variable namespace. 
  foreach ($conf as $key => $value) {
    if (strpos($key, 'node_gallery_access') === 0) {
      variable_del($key);
    }
  }
}

/**
 * Implementation of hook_update_N()
 */

function node_gallery_access_update_6201() {
  $ret = array();
  //Clean up the mess I made when I abbreviated a few variable namespaces
  $bad_varnames = array('ng_access_priority', 'ng_access_pass_image', 'ng_access_pass_gallery');
  foreach ($bad_varnames as $badvar) {
    $value = variable_get($badvar, "8675309");
    if ($value != "8675309") {
      $good_name = str_replace("ng_access_", "node_gallery_access_", $badvar);
      variable_set($good_name, $value);
      variable_del($badvar);
    }
  }
  // Since I fubared the grant realms with abbreviations, we need to rebuild them
  node_access_needs_rebuild(TRUE);
  return $ret;
}